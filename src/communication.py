import socket

from config import NAMENODE_IP, NAMENODE_PORT


def get_msg(sock, size=1024):
    response = b''
    while True:
        data = sock.recv(size)
        if data:
            response += data
        else:
            break

    return response


def get_header(data):
    header = len(data).to_bytes(4, byteorder='big', signed=False)
    return header


def send_message(command, ip=NAMENODE_IP, port=NAMENODE_PORT, sock=None, get_response=True):
    msg = command.SerializeToString()
    header = get_header(msg)
    msg = header + msg
    return send_data(msg, ip, port, sock, get_response)
    

def send_data(data, ip=NAMENODE_IP, port=NAMENODE_IP, sock=None, get_response=True):
    if sock is None:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((ip, port))
    sock.sendall(data)
    
    if get_response:
        return get_msg(sock)