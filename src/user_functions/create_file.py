from src.commands import create_unary_command
from src.communication import send_message
from protos import client_to_nn_pb2, nn_to_client_pb2


def create_file(file_p):
    command = create_unary_command(
        file_p=file_p, 
        command_type=client_to_nn_pb2.Command.COMMAND_TYPE.CREATE_FILE,
    )

    received = send_message(command)
    response = nn_to_client_pb2.Response()
    response.ParseFromString(received)

    print(response.status)