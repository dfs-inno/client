from src.communication import send_message
from protos import client_to_nn_pb2, nn_to_client_pb2


def init_dfs():
    print('Are you sure to format file system? [y/N] ', end='')
    usr_action = input().lower()
    if usr_action not in ['y', 'yes']:
        print('Cancellation')
        return

    command = client_to_nn_pb2.Command()
    command.type = client_to_nn_pb2.Command.COMMAND_TYPE.INIT

    received = send_message(command)
    response = nn_to_client_pb2.Response()
    response.ParseFromString(received)

    print(f"Free space: {int(response.status) / (2 ** 30):.4f} GiB")