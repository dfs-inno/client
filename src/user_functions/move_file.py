from src.commands import create_binary_command
from src.communication import send_message
from protos import client_to_nn_pb2, nn_to_client_pb2


def move_file(source_p, dest_p):
    command = create_binary_command(
        source_p=source_p,
        destination_p=dest_p,
        command_type=client_to_nn_pb2.Command.COMMAND_TYPE.MOVE_FILE,
    )

    received = send_message(command)
    response = nn_to_client_pb2.Response()
    response.ParseFromString(received)

    print(response.status)