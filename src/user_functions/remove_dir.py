from src.commands import create_unary_command
from src.communication import send_message
from protos import client_to_nn_pb2, nn_to_client_pb2


def remove_dir(dir_p):
    print('Are you sure to remove this dir? [y/N] ', end='')
    usr_action = input().lower()
    if usr_action not in ['y', 'yes']:
        print('Cancellation')
        return

    command = create_unary_command(
        file_p=dir_p, 
        command_type=client_to_nn_pb2.Command.COMMAND_TYPE.REMOVE_DIRECTORY,
    )

    received = send_message(command)
    response = nn_to_client_pb2.Response()
    response.ParseFromString(received)

    print(response.status)