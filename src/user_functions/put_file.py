import os
import socket

from src.commands import create_binary_command
from src.communication import send_message, get_header, send_data
from protos import client_to_nn_pb2, nn_to_client_pb2, dn_to_client_pb2, client_dn_to_dn_pb2
from config import DATANODE_PORT


def put_file(source_p, dest_p):

    command = client_to_nn_pb2.Command()
    command.type = client_to_nn_pb2.Command.COMMAND_TYPE.WRITE_FILE

    action = client_to_nn_pb2.ClientWriteFile()
    action.file_name = dest_p
    action.size = os.path.getsize(source_p)
    command.write_action.CopyFrom(action)

    received = send_message(command)
    storage_ip = nn_to_client_pb2.StorageServerIp()
    storage_ip.ParseFromString(received)

    if storage_ip.status != 'ok':
        print(storage_ip.status)
        return

    with open(source_p, 'rb') as f:
        data = f.read()

    command = client_dn_to_dn_pb2.DNCommand()
    command.type = client_dn_to_dn_pb2.DNCommand.COMMAND_TYPE.WRITE
    action = client_dn_to_dn_pb2.WriteFile()
    action.file_name = dest_p
    action.replica_number = -1
    action.size = len(data)
    command.write_file.CopyFrom(action)

    msg_bytes = command.SerializeToString()
    msg = get_header(msg_bytes) + msg_bytes + data
    received = send_data(msg, ip=storage_ip.ip, port=DATANODE_PORT)
    response = dn_to_client_pb2.DNResponse()
    response.ParseFromString(received)
    print(response)
