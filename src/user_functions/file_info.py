from src.commands import create_unary_command
from src.communication import send_message
from protos import client_to_nn_pb2, nn_to_client_pb2


def file_info(file_p):
    command = create_unary_command(
        file_p=file_p, 
        command_type=client_to_nn_pb2.Command.COMMAND_TYPE.INFO,
    )

    received = send_message(command)
    response = nn_to_client_pb2.INode()
    response.ParseFromString(received)

    if response.status == 'ok':
        msg = f"""{response.file_name}: {response.size} bytes,
        \rblocks: {response.number_of_blocks},
        \rips: {", ".join(iter(response.ips.ips))}"""
    else:
        msg = response.status
    print(msg)