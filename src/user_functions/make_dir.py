from src.commands import create_unary_command
from src.communication import send_message
from protos import client_to_nn_pb2, nn_to_client_pb2


def make_dir(dir_p):
    command = create_unary_command(
        file_p=dir_p, 
        command_type=client_to_nn_pb2.Command.COMMAND_TYPE.CREATE_DIRECTORY,
    )

    received = send_message(command)
    response = nn_to_client_pb2.Response()
    response.ParseFromString(received)

    print(response.status)