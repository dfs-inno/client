import socket
import random
from functools import partial
from multiprocessing.pool import ThreadPool

from src.commands import create_unary_command
from src.communication import send_message, get_msg
from protos import client_to_nn_pb2, nn_to_client_pb2, client_dn_to_dn_pb2
from config import DATANODE_PORT


def download_block(block_num, source_p, ips):
    ip = random.choice(ips)
    # ip = ips[0]
    command = client_dn_to_dn_pb2.DNCommand()
    command.type = client_dn_to_dn_pb2.DNCommand.COMMAND_TYPE.READ

    action = client_dn_to_dn_pb2.ReadFile()
    action.file_name = source_p
    action.chunk_number = block_num
    command.read_file.CopyFrom(action)

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect( (ip, DATANODE_PORT) )
    send_message(command, ip=ip, port=DATANODE_PORT, sock=sock, get_response=False)

    length = sock.recv(4)
    length = int.from_bytes(length, byteorder='big', signed=False)
    block = b''
    while length != len(block):
        block += sock.recv(length)
    assert length == len(block)
    sock.close()

    return (block_num, block)


def get_file(source_p, dest_p):
    command = create_unary_command(
        file_p=source_p,
        command_type=client_to_nn_pb2.Command.COMMAND_TYPE.INFO,
    )
    
    received = send_message(command)
    response = nn_to_client_pb2.INode()
    response.ParseFromString(received)

    if response.status != 'ok':
        print(response.status)
        return

    ips = response.ips.ips
    load_block = partial(download_block, source_p=source_p, ips=ips)

    with ThreadPool(10) as p:
         data_blocks = p.map(load_block, range(response.number_of_blocks))
    data_blocks = sorted(data_blocks, key=lambda x: x[0])
    data_blocks = [b for n, b in data_blocks]

    data = b''.join(data_blocks)
    with open(dest_p, 'wb') as f:
        f.write(data)
    print('Done')
