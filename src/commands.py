from protos import client_to_nn_pb2


def create_unary_command(file_p, command_type):
    command = client_to_nn_pb2.Command()
    command.type = command_type

    action = client_to_nn_pb2.UnaryAction()
    action.file_name = file_p
    command.unary_action.CopyFrom(action)

    return command


def create_binary_command(source_p, destination_p, command_type):
    command = client_to_nn_pb2.Command()
    command.type = command_type

    action = client_to_nn_pb2.BinaryAction()
    action.source_name = source_p
    action.destination_name = destination_p
    command.binary_action.CopyFrom(action)
    
    return command
