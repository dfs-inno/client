#!/usr/bin/env python

from src import parse_args
from src.user_functions import *


def main():
    args = parse_args()

    if args.init:
        init_dfs()

    elif args.touch is not None:
        create_file(args.touch)

    elif args.info is not None:
        file_info(args.info)

    elif args.rm is not None:
        remove_file(args.rm)

    elif args.put is not None:
        put_file(*args.put)

    elif args.get is not None:
        get_file(*args.get)

    elif args.cp is not None:
        copy_file(*args.cp)

    elif args.mv is not None:
        move_file(*args.mv)

    elif args.mkdir is not None:
        make_dir(args.mkdir)

    elif args.rmdir is not None:
        remove_dir(args.rmdir)

    elif args.ls is not None:
        list_dir(args.ls)

    else:
        raise "press --help"


if __name__ == "__main__":
    main()
