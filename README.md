# DFS client
Command line client for Distributed File System

## Installation
```
docker build -t dfs_client
docker run -it -e NAMENODE_IP="put here ip address" dfs_client
```

## Usage
For dfs formating use:
```
./client.py -init
```
To see all comands:
```
./client.py --help
```